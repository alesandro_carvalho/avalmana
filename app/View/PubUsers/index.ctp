	<div class="box-title">
	 	 <h3>
	 	 <i class="icon-table"></i>
	 	 Usuarios
	 	 </h3>
	</div>

	<?php echo $this->Html->link(__('Novo Usuário'), array('controller' => 'PubUsers', 'action' => 'add')); ?>

	<table cellpadding="0" cellspacing="0" class="table table-hover table-nomargin">
	<tr>

			<th><?php echo $this->Paginator->sort('login'); ?></th>

			<th><?php echo $this->Paginator->sort('perfil'); ?></th>
			<th class="actions"><?php echo __('Ações'); ?></th>
	</tr>
	<?php foreach ($users as $user): ?>
	<tr>

		<td><?php echo h($user['User']['username']); ?>&nbsp;</td>

		<td><?php echo h($user['User']['role']); ?>&nbsp;</td>

		<td class="actions">
			<?php echo $this->Html->link(__('Visulizar'), array('controller' => 'PubUsers', 'action' => 'view', $user['User']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('controller' => 'PubUsers', 'action' => 'edit', $user['User']['id'])); ?>
			<?php echo $this->Form->postLink(__('Excluir'), array('controller' => 'PubUsers', 'action' => 'delete', $user['User']['id']), null, __('Confirma a exclusão # %s?')); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Página {:page} de {:pages}, visualizando {:current} registros de {:count} total, começando pelo registro {:start}, finalizando em {:end}')
	));
	?>	</p>
	<div>
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('próximo') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
