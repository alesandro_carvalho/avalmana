<div>

<?php echo $this->Session->flash('auth'); ?>

<?php echo $this->Form->create('User');?>
    <fieldset>

          <?php echo $this->Form->input('username', array('class'=>"input-text",'placeholder'=>"Endereço de E-mail",
                                        'label' => 'Login', 'div' => array('style' => ' clear: both; width: 255px; float: left;'))) ."\n"; ?>

          <?php echo $this->Form->input('password',array('class'=>"input-text",'data-rule-required'=>"true",
                                        'label' => 'Senha','placeholder'=>"Senha",
                                         'div' => array('style' => ' width: 255px; float: left;'))) ."\n"; ?>
        <div class="clear"></div>
        <p class="form-row">
          <?php echo $this->Form->end(array('class'=>"btn btn-primary",'label'=>"Entrar",
          'div' => array('style' => ' width: 255; float: left;')));?>

          <?php
            echo $this->Html->image(
              'facebook.gif',
              array(
                'title' => 'Facebook Login',
                'url' => ($fb_login_url)
              )
            );
          ?>



          <?php echo $this->Html->link('Esqueceu a senha?', array('controller' => 'PubUsers', 'action' => 'edit')); ?>
        </p>



    </fieldset>

</div>
