<?php
App::uses('AppController', 'Controller');

class PubEvaluationsController extends AppController {
    public $helpers = array ('Html','Form');
    public $components = array('Session','Paginator');


    public function beforeFilter() {
	    parent::beforeFilter();
	    $this->Auth->allow('add', 'logout');
	    $this->loadModel('Rererence');
	}

	public function index() {
	    $this->Paginator->settings = array(
	        'conditions' => array('Evaluation.id =' => $this->Auth->Evaluation('id')),
	        'limit' => 10
	    );

	    $data = $this->Paginator->paginate('Evaluation');

		$this->set('Evaluations',$data);  
 	}

	public function view($id = null) {
		$this->verifyExists('Evaluation',$id);
		$options = array('conditions' => array('Evaluation.' . $this->Evaluation->primaryKey => $id));
		$this->loadEvaluation($id);
	}

	public function add() {
		if ($this->isSubmitedForm()) {
			$datasource = $this->Evaluation->getDataSource(); 
			$datasource->begin();

			try{
				$this->Evaluation->create();
				$this->Evaluation->save($this->request->data);
				$datasource->commit();
				$this->redirectWithFlash('Avaliação salvo com sucesso!', 'index');
			}catch(Exception $e){
				$datasource->rollback();
				$this->printFlash('Ocorreu um erro ao salvar dados do Avaliação. Por favor tente novamente.');
			}
		}

        $references = $this->Reference->find('list');
        $this->set(compact('references'));         		
	}

	public function edit($id = null) {
		$this->verifyExists('Evaluation',$id);

		if ($this->isSubmitedForm()) {
			$datasource = $this->Evaluation->getDataSource(); 
			$datasource->begin();

			try{
				$this->Evaluation->save($this->request->data);
				$datasource->commit();
				$this->redirectWithFlash('Alteração realizada com sucesso!', 'index');
			}catch(Exception $e){
				$datasource->rollback();
				$this->printFlash('Ocorreu um erro ao salvar dados da avaliação. Por favor tente novamente');
			}
		}else{
			$this->request->data = $this->Evaluation->findById($id);
		}

        $Evaluations = $this->Evaluation->find('list');
        $this->set(compact('Evaluations'));   		
	}

	public function delete($id = null) {
		$this->Evaluation->id = $id;
		$this->verifyExists('Evaluation',$id);
		$this->request->onlyAllow('post', 'delete');

		$datasource = $this->Evaluation->getDataSource(); 
		$datasource->begin();		
		
		try{
			$this->Evaluation->delete();
			$datasource->commit();
			$this->redirectWithFlash('Avaliação excluída!', 'index');
		}catch(Exception $e){
			$datasource->rollback();
			$this->redirectWithFlash('Avaliação não pode ser excluída', 'index');
		}
	}

	public function isAuthorized($Evaluation){

        if(parent::isAuthorized($Evaluation)){
        	return true;
        }

		if (in_array($this->action, array('index','view','edit','delete','logout'))){
			
			$EvaluationId = $this->request->params['pass'][0];

			if ($this->Evaluation->isOwnedBy($EvaluationId, $Evaluation['id'])){
				return true;
			}else{
				$this->alert('Acesso não permitido!');	
				return false;
			}
		}

	}

	public function loadEvaluation($id){
		$Evaluation = $this->Evaluation->findById($id);
		$this->set('Evaluation', $Evaluation);
	}

}
