<?php

class PubLoginController extends AppController{

    public $helpers = array ('Html','Form');
    public $components = array('Session');

	public function beforeFilter() {
	    parent::beforeFilter();
	    $this->loadModel('User');
        $this->layout = "loginmana";
	}

	public function login(){
		
		if ($this->request->isPost()){
			if($this->Auth->login()){
				$user = $this->Auth->user();
				
				if ($user['role'] != 'client'){
					$this->alert('Area reservada apenas para clientes!');
					return false;
				}

	            $this->success("Logado com sucesso.");
				$this->redirect($this->Auth->redirect());
			}

	       if($this->isSubmitedForm()){
	           $this->alert('Erro ao fazer login, verifique sua senha e password.');
	        }
    	}
	 	// When facebook login is used, facebook always returns $_GET['code'].
	    else if($this->request->query('code')){

	        // User login successful
	        $fb_user = $this->Facebook->getUser();          # Returns facebook user_id
	        if ($fb_user){
	            $fb_user = $this->Facebook->api('/me');     # Returns user information

	            // We will varify if a local user exists first
	            $local_user = $this->User->find('first', array(
	                'conditions' => array('facebook_id' => $fb_user['id'])
	            ));

	            // If exists, we will log them in
	            if ($local_user){
	                $this->Auth->login($local_user['User']);            # Manual Login
	                $this->redirect($this->Auth->redirectUrl());
	            } 

	            // Otherwise we ll add a new user (Registration)
	            else {
	                $data['User'] = array(
	                    'username'      => $fb_user['username'],                              # Normally Unique
	                    'facebook_id'      => $fb_user['id'],  
	                    'password'      => AuthComponent::password(uniqid(md5(mt_rand()))), # Set random password
	                    'role'          => 'client'
	                );

	                // You should change this part to include data validation
	                $this->User->save($data, array('validate' => false));

	                // After registration we will redirect them back here so they will be logged in
	                $this->redirect(Router::url('/PubLogin/login?code=true', true));
	            }
	        }

	        else{
	            $this->alert('Erro ao fazer login, verifique sua senha e password.');
	        }
	    }    	
	}

	public function logout(){
		$this->redirect($this->Auth->logout());
	}

	public function index(){
		parent::index();
	}

}

?>