<?php
App::uses('AppController', 'Controller');

class PubUsersController extends AppController {
    public $helpers = array ('Html','Form');
    public $components = array('Session','Paginator');


    public function beforeFilter() {
	    parent::beforeFilter();
	    $this->Auth->allow('add', 'logout');
	    $this->loadModel('User');
	}

	public function index() {
	    $this->Paginator->settings = array(
	        'conditions' => array('User.id =' => $this->Auth->user('id')),
	        'limit' => 10
	    );

	    $data = $this->Paginator->paginate('User');

		$this->set('users',$data);  
 	}

	public function view($id = null) {
		$this->verifyExists('User',$id);
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->loadUser($id);
	}

	public function add() {
		if ($this->isSubmitedForm()) {
			$datasource = $this->User->getDataSource(); 
			$datasource->begin();

			try{
				$this->User->create();
				$this->User->save($this->request->data);
				$datasource->commit();
				$this->redirectWithFlash('Usuário salvo com sucesso!', 'index');
			}catch(Exception $e){
				$datasource->rollback();
				$this->printFlash('Ocorreu um erro ao salvar dados do usuário. Por favor tente novamente.');
			}
		}

        $users = $this->User->find('list');
        $this->set(compact('users'));   		
	}

	public function edit($id = null) {
		$this->verifyExists('User',$id);

		if ($this->isSubmitedForm()) {
			$datasource = $this->User->getDataSource(); 
			$datasource->begin();

			try{
				$this->User->save($this->request->data);
				$datasource->commit();
				$this->redirectWithFlash('Alteração realizada com sucesso!', 'index');
			}catch(Exception $e){
				$datasource->rollback();
				$this->printFlash('Ocorreu um erro ao salvar dados do usuário. Por favor tente novamente');
			}
		}else{
			$this->request->data = $this->User->findById($id);
		}

        $users = $this->User->find('list');
        $this->set(compact('users'));   		
	}

	public function delete($id = null) {
		$this->User->id = $id;
		$this->verifyExists('User',$id);
		$this->request->onlyAllow('post', 'delete');

		$datasource = $this->User->getDataSource(); 
		$datasource->begin();		
		
		try{
			$this->User->delete();
			$datasource->commit();
			$this->redirectWithFlash('Usuário excluído!', 'index');
		}catch(Exception $e){
			$datasource->rollback();
			$this->redirectWithFlash('Usuário não pode ser excluído', 'index');
		}
	}

	public function isAuthorized($user){

        if(parent::isAuthorized($user)){
        	return true;
        }

		if (in_array($this->action, array('index','view','edit','delete','logout'))){
			
			$userId = $this->request->params['pass'][0];

			if ($this->User->isOwnedBy($userId, $user['id'])){
				return true;
			}else{
				$this->alert('Acesso não permitido!');	
				return false;
			}
		}

	}

	public function loadUser($id){
		$user = $this->User->findById($id);
		$this->set('user', $user);
	}

	public function resetpassword($id=null){
		$this->verifyExists('User',$id);

		if ($this->isSubmitedForm()) {
			try{
				$datasource = $this->User->getDataSource(); 
				$datasource->begin();
				$this->User->save($this->request->data, $params = array('fieldList' => array('password')));
				$datasource->commit();
				$this->redirectWithFlash('Senha Alterada com sucesso!', 'index');
			}catch(Exception $e){	
				$datasource->rollback();
				$this->alert('Ocorreu um erro ao alterar a senha!');
			}
		}else{
			$this->request->data = $this->User->findById($id);
		}

        $users = $this->User->find('list');
        $this->set(compact('users'));		
	}
}
