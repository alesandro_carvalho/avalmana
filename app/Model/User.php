<?php

App::uses('AppModel', 'Model');

class User extends AppModel{
    public $name = 'User';

    public $displayField = 'username';

    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe o nome do usuário'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe sua senha de acesso'
            )
        ),
        'role' => array(
            'required' => array(
                'rule' => array('inList', array('admin', 'company')),
                'message' => 'Selecione um perfil válido',
                'allowEmpty' => false
            )
        )
    );

    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
        return true;
    }

    public function isOwnedBy($userPass, $userLogado){
        return $this->field('id', array('id' => $userPass)) === $userLogado;
    }


}

?>