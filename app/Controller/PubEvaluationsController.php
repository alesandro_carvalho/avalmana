<?php
App::uses('AppController', 'Controller');
	  
class PubEvaluationsController extends AppController {
    public $helpers = array ('Html','Form');
    public $components = array('Session','Paginator');
    public $uses = 'Evaluation';
    public $name = 'PubEvaluations';


    public function beforeFilter() {
	    parent::beforeFilter();
	    $this->loadModel('Reference');
	    $this->loadModel('Entry');
	    $this->Auth->allow('add', 'logout');
	}

	public function index() {
		$this->Evaluation->recursive = 0;
		$this->set('evaluations', $this->Paginator->paginate());

	    /*$this->Paginator->settings = array(
	        'conditions' => array('Evaluation.user_id =' => $this->Auth->user('id')),
	        'limit' => 10
	    );

	    $data = $this->Paginator->paginate('Evaluation');

		$this->set('evaluations', $data); */

		$this->compactModels();
 	}

	public function view($id = null) {
		$this->verifyExists('Evaluation',$id);
		$options = array('conditions' => array('Evaluation.' . $this->Evaluation->primaryKey => $id));
		$this->loadEvaluation($id);
	}

	public function add() {
		if ($this->isSubmitedForm()) {
			$datasource = $this->Evaluation->getDataSource(); 
			$datasource->begin();

			try{
				$this->Evaluation->create();
				$this->Evaluation->save($this->request->data);
				$datasource->commit();
				$this->redirectWithFlash('Avaliação salvo com sucesso!', 'index');
			}catch(Exception $e){
				$datasource->rollback();
				$this->alert('Ocorreu um erro ao salvar dados do Avaliação. Por favor tente novamente.');
			}
		}

		$this->compactModels();
	}	

	public function edit($id = null) {
		$this->verifyExists('Evaluation',$id);

		if ($this->isSubmitedForm()) {
			$datasource = $this->Evaluation->getDataSource(); 
			$datasource->begin();

			try{
				$this->Evaluation->save($this->request->data);
				$datasource->commit();
				$this->redirectWithFlash('Alteração realizada com sucesso!', 'index');
			}catch(Exception $e){
				$datasource->rollback();
				$this->alert('Ocorreu um erro ao salvar dados da avaliação. Por favor tente novamente');
			}
		}else{
			$this->request->data = $this->Evaluation->findById($id);
		}

        $this->compactModels();  		
	}

	public function delete($id = null) {
		$this->Evaluation->id = $id;
		$this->verifyExists('Evaluation',$id);
		$this->request->onlyAllow('post', 'delete');

		$datasource = $this->Evaluation->getDataSource(); 
		$datasource->begin();		
		
		try{
			$this->Evaluation->delete();
			$datasource->commit();
			$this->redirectWithFlash('Avaliação excluída!', 'index');
		}catch(Exception $e){
			$datasource->rollback();
			$this->redirectWithFlash('Avaliação não pode ser excluída', 'index');
		}
	}

	public function isAuthorized($Evaluation){

        if(parent::isAuthorized($Evaluation)){
        	return true;
        }

		if (in_array($this->action, array('index','view','edit','delete','logout'))){
			
			$EvaluationId = $this->request->params['pass'][0];

			if ($this->Evaluation->isOwnedBy($EvaluationId, $Evaluation['id'])){
				return true;
			}else{
				$this->alert('Acesso não permitido!');	
				return false;
			}
		}

	}

	public function loadEvaluation($id){
		$Evaluation = $this->Evaluation->findById($id);
		$this->set('Evaluation', $Evaluation);
	}

	private function compactModels(){
		$products = $this->Evaluation->Product->find('list');
		$cleanings = $this->Evaluation->Cleaning->find('list');
		$treatments = $this->Evaluation->Treatment->find('list');
		$disclosures = $this->Evaluation->Disclosure->find('list');
		$organizations = $this->Evaluation->Organization->find('list');
		$securities = $this->Evaluation->Security->find('list');
		$environments = $this->Evaluation->Environment->find('list');
		$locations = $this->Evaluation->Location->find('list');
		$prices = $this->Evaluation->Price->find('list');
		$agilities = $this->Evaluation->Agility->find('list');
		$entries = $this->Evaluation->Entry->find('list');


		$this->set(compact('products','cleanings','treatments','disclosures','organizations',
						    'securities','environments','locations','prices','agilities','entries'));		
	}

}
