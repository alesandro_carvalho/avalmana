<?php

App::uses('AppModel', 'Model');

Class Entry extends AppModel{

    public function all($controller){
    	return $controller->Entry->find('all',array('conditions' => array('user_id' => 16)),array('order'=>'id desc'));
    }    

    public function isOwnedBy($entry, $user) {
        return $this->field('id', array('id' => $entry, 'user_id' => $user)) === (int)$entry;
    }

}


?>