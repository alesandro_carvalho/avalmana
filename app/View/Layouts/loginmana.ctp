<html>
<head>
	<title>FLAT - Login</title>
	<?php echo $this->Html->charset(); ?>
	<?php echo CSS."Pagebuilder/css/?ver=3.5.1"; ?>

	<!-- css -->
	<link rel='stylesheet' id='blox-style-css' href='../app/webroot/css/Pagebuilder/css/?ver=3.5.1' type='text/css' media='all'/>	
	<?php
		echo $this->Html->css('bootstrap');
		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('bootstrap-nonresponsive');
		echo $this->Html->css('common');
		echo $this->Html->css('ie8');
		echo $this->Html->css('responsive.css');
		echo $this->Html->css('woocommerce-custom-style');
		echo $this->Html->css('layerslider');
		echo $this->Html->css('style');
		//echo $this->Html->css('icheck/all');
		//echo $this->Html->css('cake.generic');
	?>

	<!-- js -->
	<?php
		echo $this->Html->script('jquery');
		echo $this->Html->script('jquery.fitvids');
		echo $this->Html->script('jquery.isotope.min');
		echo $this->Html->script('jqueryslidemenu');
		echo $this->Html->script('scripts');
		echo $this->Html->script('common');	
		echo $this->Html->script('html5shiv');
		echo $this->Html->script('metrize-ie7');
		echo $this->Html->script('respond.min');
		echo $this->Html->script('skrollr.min');
		echo $this->Html->script('waypoints.min');
	?>

</head></head>

<body>

<div id="feature" permalink="http://themes.themeton.com/mana/my-account/" class="light">
 
<div class="container">
<div class="row">
<div class="col-xs-12 col-md-12 col-lg-9 col-sm-12">
<h1 class="page_title">
	<!--<?php echo $this->Html->image('logo.jpg', array('alt' => 'seuAval', 'width'=>'200')); ?>-->
	seuAval
</h1>
</div>
</div>
</div>
</div>
 
</div>

	<section id="content" class="section-420  ">
	 	<div clas="login">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
						<div id="primary" class="content">
							<article>
								<div class="entry_content">
									<div class="woocommerce">
									<h2 class="woo_title">Informações de Login</h2>
				                        <?php echo $this->element("messages"); ?>
				                        <?php echo $this->fetch("content"); ?>
									</div>								
									</div>
								</div>
							</article>
						</div> 
					</div> 
				</div> 
			</div> 
		</div>
	</section>

</body>
</html>