<?php $success = $this->Session->Flash("success");?>
<?php if($success): ?>
	<ul class="woocommerce-info" onclick="$('.woocommerce-info').hide();">
	<li><strong>INFO</strong>: <?php echo $success; ?></li>
	</ul>
<?php endif; ?>

<?php $error = $this->Session->Flash("error"); ?>
<?php if($error): ?>
	<ul class="woocommerce-error" onclick="$('.woocommerce-error').hide();">
	<li><strong>ERROR</strong>: <?php echo $error; ?></li>
	</ul>

<?php endif; ?>
