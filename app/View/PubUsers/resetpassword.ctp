<div>
	<div class="box-title">
	  <h3>
	    <i class="icon-table"></i>
	     Alterar Senha
	  </h3>
	</div>

	<?php echo $this->Form->create('User'); ?>
	<fieldset>

		<div class="control-group">
			<?php echo $this->Form->input('id', array('type' => 'hidden')); ?>
		</div>
		<div class="control-group">
			<?php echo $this->Form->input('username', array('class'=>"input-block-level",'placeholder'=>"Login",
														'label'=>"Login", 'disabled' => 'true', 'div'=>array('class'=>"pw controls"))); ?>			
		</div>
		<div class="control-group">
				<?php echo $this->Form->input('password', array('class'=>"input-block-level",'placeholder'=>"Senha",
														'label'=>"Senha", 'div'=>array('class'=>"pw controls"),
														'value'=>'')); ?>	
		</div>

	</fieldset>

	<div class="submit">
		<?php echo $this->Form->end(array('class'=>"btn btn-primary", 'label'=>'Salvar')); ?>
	</div>


</div>