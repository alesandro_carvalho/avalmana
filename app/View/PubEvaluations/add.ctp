<div class="evaluations form">
<?php echo $this->Form->create('Evaluation'); ?>
	<fieldset>
		<legend><h1><?php echo __('Avaliar Estabelecimento'); ?></h1></legend>

		<?php echo $this->Form->input('entries',array('label' => 'Estabelecimento')); ?>

		<br/><br/>
		<?php echo $this->Form->radio('products',$products, array('legend' => 'Produtos')); ?></div>

		<div class="row " id="accordion">
			<div class="col-xs-12 col-xxs-6 col-sm-6 col-md-6 col-lg-6">
				<div class='blox_element tt_accordion   bordered'>
					<div class="accordion_title ">
						<a href="#" title=""><i class='icon-user'></i>Justifique Sua Avaliação</a>
						<span class="accordion_arrows"><i class="icon-chevron-down"></i><i class="icon-chevron-up"></i></span>
					</div>
					<div class="accordion_content">
						<div class='blox_element tt_text_content  '>
					 		<?php echo $this->Form->input('whyproducts',array('label' => 'Justificativa', 'cols' => 120, 'rows' => 10)); ?>
						</div>
					</div>	
				</div>
			</div>
		</div>		

	<?php echo $this->Form->radio('cleaning',$cleanings,array('legend' => 'Limpeza')); ?>
	
		<div class="row " id="accordion">
			<div class="col-xs-12 col-xxs-6 col-sm-6 col-md-6 col-lg-6">
				<div class='blox_element tt_accordion   bordered'>
					<div class="accordion_title ">
						<a href="#" title=""><i class='icon-user'></i>Justifique Sua Avaliação</a>
						<span class="accordion_arrows"><i class="icon-chevron-down"></i><i class="icon-chevron-up"></i></span>
					</div>
					<div class="accordion_content">
						<div class='blox_element tt_text_content  '>
					 		<?php echo $this->Form->input('whycleaning',array('label' => 'Justificativa', 'cols' => 120, 'rows' => 10)); ?>
						</div>
					</div>	
				</div>
			</div>
		</div>	
	
	<?php echo $this->Form->radio('treatment',$treatments,array('legend'=> 'Tratamento')); ?>
		<div class="row " id="accordion">
			<div class="col-xs-12 col-xxs-6 col-sm-6 col-md-6 col-lg-6">
				<div class='blox_element tt_accordion   bordered'>
					<div class="accordion_title ">
						<a href="#" title=""><i class='icon-user'></i>Justifique Sua Avaliação</a>
						<span class="accordion_arrows"><i class="icon-chevron-down"></i><i class="icon-chevron-up"></i></span>
					</div>
					<div class="accordion_content">
						<div class='blox_element tt_text_content  '>
					 		<?php echo $this->Form->input('whytreatment',array('label' => 'Justificativa', 'cols' => 120, 'rows' => 10)); ?>
						</div>
					</div>	
				</div>
			</div>
		</div>		



		<?php echo $this->Form->radio('disclosure',$disclosures,array('legend' =>'Divulgação')); ?>

		<div class="row " id="accordion">
			<div class="col-xs-12 col-xxs-6 col-sm-6 col-md-6 col-lg-6">
				<div class='blox_element tt_accordion   bordered'>
					<div class="accordion_title ">
						<a href="#" title=""><i class='icon-user'></i>Justifique Sua Avaliação</a>
						<span class="accordion_arrows"><i class="icon-chevron-down"></i><i class="icon-chevron-up"></i></span>
					</div>
					<div class="accordion_content">
						<div class='blox_element tt_text_content  '>
					 		<?php echo $this->Form->input('whydisclosure',array('label' => 'Justificativa', 'cols' => 120, 'rows' => 10)); ?>
						</div>
					</div>	
				</div>
			</div>
		</div>		

		
		<?php echo $this->Form->radio('organization',$organizations, array('legend' => 'Organização')); ?>

		<div class="row " id="accordion">
			<div class="col-xs-12 col-xxs-6 col-sm-6 col-md-6 col-lg-6">
				<div class='blox_element tt_accordion   bordered'>
					<div class="accordion_title ">
						<a href="#" title=""><i class='icon-user'></i>Justifique Sua Avaliação</a>
						<span class="accordion_arrows"><i class="icon-chevron-down"></i><i class="icon-chevron-up"></i></span>
					</div>
					<div class="accordion_content">
						<div class='blox_element tt_text_content  '>
					 		<?php echo $this->Form->input('whyorganization',array('label' => 'Justificativa', 'cols' => 120, 'rows' => 10)); ?>
						</div>
					</div>	
				</div>
			</div>
		</div>		

		
		<?php echo $this->Form->radio('security',$securities, array('legend' => 'Segurança')); ?>
		<div class="row " id="accordion">
			<div class="col-xs-12 col-xxs-6 col-sm-6 col-md-6 col-lg-6">
				<div class='blox_element tt_accordion   bordered'>
					<div class="accordion_title ">
						<a href="#" title=""><i class='icon-user'></i>Justifique Sua Avaliação</a>
						<span class="accordion_arrows"><i class="icon-chevron-down"></i><i class="icon-chevron-up"></i></span>
					</div>
					<div class="accordion_content">
						<div class='blox_element tt_text_content  '>
					 		<?php echo $this->Form->input('whysecurity',array('label' => 'Justificativa', 'cols' => 120, 'rows' => 10)); ?>
						</div>
					</div>	
				</div>
			</div>
		</div>

		<?php echo $this->Form->radio('environment',$environments, array('legend' => 'Ambiente e Disposição')); ?>
		<div class="row " id="accordion">
			<div class="col-xs-12 col-xxs-6 col-sm-6 col-md-6 col-lg-6">
				<div class='blox_element tt_accordion   bordered'>
					<div class="accordion_title ">
						<a href="#" title=""><i class='icon-user'></i>Justifique Sua Avaliação</a>
						<span class="accordion_arrows"><i class="icon-chevron-down"></i><i class="icon-chevron-up"></i></span>
					</div>
					<div class="accordion_content">
						<div class='blox_element tt_text_content  '>
					 		<?php echo $this->Form->input('whyenvironment',array('label' => 'Justificativa', 'cols' => 120, 'rows' => 10)); ?>
						</div>
					</div>	
				</div>
			</div>
		</div>

		
		<?php echo $this->Form->radio('location',$locations,array('legend' => 'Localização ou entrega')); ?>

		<?php echo $this->Form->input('environment',array('label' => 'Ambiente e Disposição')); ?>
		<div class="row " id="accordion">
			<div class="col-xs-12 col-xxs-6 col-sm-6 col-md-6 col-lg-6">
				<div class='blox_element tt_accordion   bordered'>
					<div class="accordion_title ">
						<a href="#" title=""><i class='icon-user'></i>Justifique Sua Avaliação</a>
						<span class="accordion_arrows"><i class="icon-chevron-down"></i><i class="icon-chevron-up"></i></span>
					</div>
					<div class="accordion_content">
						<div class='blox_element tt_text_content  '>
					 		<?php echo $this->Form->input('whylocation',array('label' => 'Justificativa', 'cols' => 120, 'rows' => 10)); ?>
						</div>
					</div>	
				</div>
			</div>
		</div>

		<?php echo $this->Form->radio('price',$prices,array('legend' => 'Preço ou forma de pagamento')); ?>

		<div class="row " id="accordion">
			<div class="col-xs-12 col-xxs-6 col-sm-6 col-md-6 col-lg-6">
				<div class='blox_element tt_accordion   bordered'>
					<div class="accordion_title ">
						<a href="#" title=""><i class='icon-user'></i>Justifique Sua Avaliação</a>
						<span class="accordion_arrows"><i class="icon-chevron-down"></i><i class="icon-chevron-up"></i></span>
					</div>
					<div class="accordion_content">
						<div class='blox_element tt_text_content  '>
					 		<?php echo $this->Form->input('whyprice',array('label' => 'Justificativa', 'cols' => 120, 'rows' => 10)); ?>
						</div>
					</div>	
				</div>
			</div>
		</div>


		<?php echo $this->Form->radio('agility',$agilities,array('legend' => 'Agilidade ou Prazo')); ?>
		<div class="row " id="accordion">
			<div class="col-xs-12 col-xxs-6 col-sm-6 col-md-6 col-lg-6">
				<div class='blox_element tt_accordion   bordered'>
					<div class="accordion_title ">
						<a href="#" title=""><i class='icon-user'></i>Justifique Sua Avaliação</a>
						<span class="accordion_arrows"><i class="icon-chevron-down"></i><i class="icon-chevron-up"></i></span>
					</div>
					<div class="accordion_content">
						<div class='blox_element tt_text_content  '>
					 		<?php echo $this->Form->input('whyagility',array('label' => 'Justificativa', 'cols' => 120, 'rows' => 10)); ?>
						</div>
					</div>	
				</div>
			</div>
		</div>


	</fieldset>
<?php echo $this->Form->end(__('Salvar')); ?>
</div>

<?php
	echo $this->Html->script('blox-frontend');
?>