<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>

	<!-- css -->
	<link rel='stylesheet' id='blox-style-css' href='../app/webroot/css/Pagebuilder/css/?ver=3.5.1' type='text/css' media='all'/>
	<?php
		echo $this->Html->css('bootstrap');
		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('bootstrap-nonresponsive');
		echo $this->Html->css('common');
		echo $this->Html->css('ie8');
		echo $this->Html->css('responsive.css');
		echo $this->Html->css('woocommerce-custom-style');
		echo $this->Html->css('layerslider');
		echo $this->Html->css('style');
		//echo $this->Html->css('static-captions');
		//echo $this->Html->css('captions-original');
		//echo $this->Html->css('cake.generic');
	?>

	<!-- js -->
	<?php
		//echo $this->Html->script('jquery');
		echo $this->Html->script('accordion');
		echo $this->Html->script('jquery-2.1.0');
		echo $this->Html->script('jquery.fitvids');
		echo $this->Html->script('jquery.isotope.min');
		echo $this->Html->script('jqueryslidemenu');
		echo $this->Html->script('bootstrap.min');
		echo $this->Html->script('common');	
		echo $this->Html->script('html5shiv');
		echo $this->Html->script('metrize-ie7');
		echo $this->Html->script('respond.min');
		echo $this->Html->script('skrollr.min');
		echo $this->Html->script('waypoints.min');
	?>

</head>
<body>
<div id="feature" permalink="http://themes.themeton.com/mana/blog/big-blog/" class="light">
 
<div class="container">
<div class="row">
<div class="col-xs-12 col-md-12 col-lg-9 col-sm-12">
	<?php echo $this->Html->image('logo.jpg', array('alt' => 'SeuAval', 'width' => '100')); ?>
</div>
<div class="col-xs-12 col-md-12 col-lg-3 col-sm-12">
	<div id="crumbs" class="tt_breadcrumb">
		<span><?php echo $usuario['username'];  ?></span>
		<span><?php echo $this->Html->link(__('LOGOUT'), array('controller' => 'PubLogin', 'action' => 'Logout')); ?></span>
	</div>
</div>
</div>
</div>
 
</div>		
		
	</div>	 
	<div class="container">
	<div class="row">

	</div> 
	</div> 
	</div>

<header id="header" class="">
 
<div class="container">
<div class="row">

<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
<div class="default_menu align_right">
<nav class="mainmenu hidden-xs hidden-sm visible-md visible-lg">
<ul class="menu">
	<li class="menu-item-2402 page_item_has_children">
		<span class="menu_text"><?php echo $this->Html->link(__('Avaliar Estabelecimento'), array('controller' => 'PubEvaluations', 'action' => 'add')); ?></span>
		</a>
	</li>
</ul>

</nav> 
</div>
<a href="http://themes.themeton.com/mana/cart/" class="show-mobile-cart visible-xs visible-sm hidden-md hidden-lg"><i class="icon-shopping-cart"></i></a> <a id="mobile-menu-expand-collapse" href="#" class="show-mobile-menu hidden-md hidden-lg"></a></div>
</div> 
 
</header>





    <div class="box-content">
      <?php echo $this->element("messages"); ?>
      <?php echo $this->fetch("content"); ?>
    </div>

</body>
</html>
