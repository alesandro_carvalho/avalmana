<div class="evaluations view">
<h2><?php  echo __('Evaluation'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($evaluation['Evaluation']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Qualifications'); ?></dt>
		<dd>
			<?php echo h($evaluation['Evaluation']['qualifications']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cleaning'); ?></dt>
		<dd>
			<?php echo h($evaluation['Evaluation']['cleaning']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Treatment'); ?></dt>
		<dd>
			<?php echo h($evaluation['Evaluation']['treatment']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Disclosure'); ?></dt>
		<dd>
			<?php echo h($evaluation['Evaluation']['disclosure']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Organization'); ?></dt>
		<dd>
			<?php echo h($evaluation['Evaluation']['organization']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Security'); ?></dt>
		<dd>
			<?php echo h($evaluation['Evaluation']['security']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Environment'); ?></dt>
		<dd>
			<?php echo h($evaluation['Evaluation']['environment']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Location'); ?></dt>
		<dd>
			<?php echo h($evaluation['Evaluation']['location']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price'); ?></dt>
		<dd>
			<?php echo h($evaluation['Evaluation']['price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Agility'); ?></dt>
		<dd>
			<?php echo h($evaluation['Evaluation']['agility']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Evaluation'), array('action' => 'edit', $evaluation['Evaluation']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Evaluation'), array('action' => 'delete', $evaluation['Evaluation']['id']), null, __('Are you sure you want to delete # %s?', $evaluation['Evaluation']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Evaluations'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Evaluation'), array('action' => 'add')); ?> </li>
	</ul>
</div>
