<div class="evaluations form">
<?php echo $this->Form->create('Evaluation'); ?>
	<fieldset>
		<legend><?php echo __('Edit Evaluation'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('qualifications');
		echo $this->Form->input('cleaning');
		echo $this->Form->input('treatment');
		echo $this->Form->input('disclosure');
		echo $this->Form->input('organization');
		echo $this->Form->input('security');
		echo $this->Form->input('environment');
		echo $this->Form->input('location');
		echo $this->Form->input('price');
		echo $this->Form->input('agility');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Evaluation.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Evaluation.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Evaluations'), array('action' => 'index')); ?></li>
	</ul>
</div>
