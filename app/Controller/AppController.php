<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

public $components = array(
        'DebugKit.Toolbar',
        'Session',
        'Auth' => array(
            'loginRedirect'  => array('controller' => 'PubEvaluations', 'action' => 'index'),
            'logoutRedirect' => array('controller' => 'PubLogin', 'action' => 'login'),
            'loginAction'    => array('controller' => 'PubLogin', 'action' => 'login'),
            'authorize' => array('Controller')
        )
    );

    public function isAuthorized($user){
        /*qualquer perfil tem acesso a logout*/
        if (($this->action == 'logout') ){
            return true;
        }

        return false;
    }

    public function beforeRender (){
        $user = $this->Auth->user();
        $this->set('usuario', $user);

        $this->set('fb_login_url', $this->Facebook->getLoginUrl(array('redirect_uri' => Router::url(array('controller' => 'PubLogin', 'action' => 'login'), true))));
        $this->set('user', $this->Auth->user()); 
    }    

    public function beforeFilter() {
        App::import('Vendor', 'facebook-php-sdk-master/src/facebook');
        $this->Facebook = new Facebook(array(
            'appId'     =>  '1410883859165588',
            'secret'    =>  '22e9d34914cf6dbbf82616fb17bf14ec'

        ));

        $this->Auth->allow('index','logout');
    }

    public function isSubmitedForm(){
        return $this->request->is('post') || $this->request->is('put');
    }

    public function redirectWithFlash($message,$action,$type="success"){
        $this->Session->setFlash(__($message));
        if($type !== "success"){
            $this->alert(__($message));
        }
        else{
            $this->success(__($message));
        }
        $this->redirect(array('action' => $action));
    }

    public function alert($msg){
        $this->Session->setFlash($msg, 'default', array(), 'error');
    }

    public function success($msg) {
        $this->Session->setFlash($msg, 'default', array(), 'success');
    }

    public function verifyExists($model, $id){
        if (!$this->$model->exists($id)) {
            throw new NotFoundException(__('Registro inválido'));
        }
    }

    public function getUserLogin(){
        return $this->Auth->user();      
    }
}
