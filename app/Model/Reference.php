<?php
App::uses('AppModel', 'Model');

class Reference extends AppModel{
    public $name = 'Reference';

    public $displayField = 'description';

}

?>