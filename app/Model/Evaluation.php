<?php
App::uses('AppModel', 'Model');

class Evaluation extends AppModel{

    public $useTable = 'evaluations';

	public $belongsTo = array(
		'Entry' => array(
			'className' => 'Entry',
			'foreignKey' => 'entry_id'			
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'			
		),
		'Product' => array(
			'className' => 'Reference',
			'foreignKey' => 'products'
		),
		'Cleaning' => array(
			'className' => 'Reference',
			'foreignKey' => 'cleaning'
		),	
		'Treatment' => array(
			'className' => 'Reference',
			'foreignKey' => 'treatment'
		),
		'Disclosure' => array(
			'className' => 'Reference',
			'foreignKey' => 'disclosure'
		),
		'Organization' => array(
			'className' => 'Reference',
			'foreignKey' => 'organization'
		),
		'Security' => array(
			'className' => 'Reference',
			'foreignKey' => 'security'
		),
		'Environment' => array(
			'className' => 'Reference',
			'foreignKey' => 'environment'
		),
		'Location' => array(
			'className' => 'Reference',
			'foreignKey' => 'location'
		),
		'Price' => array(
			'className' => 'Reference',
			'foreignKey' => 'price'
		),
		'Agility' => array(
			'className' => 'Reference',
			'foreignKey' => 'agility'
		)
	);  

}

?>