<div>
	<h2 itemprop="headline"><?php echo __('Avaliações'); ?></h2>


	<?php foreach ($evaluations as $evaluation): ?>

		<article class="entry format_gallery blog_medium medium_top_image post_filter_item filter-big-blog filter-grid-layout filter-image-layout filter-masonry-layout  clearfix">
		<div class="blox_element blox_gallery gallery_layout_slider">

		</div> <div class="entry_title">
		<h2 itemprop="headline"><?php echo h($evaluation['Entry']['title']); ?></h2>
		</div>
		<div class="entry_meta">
		<ul class="top_meta">
		<li class="meta_author">BY <?php echo h($evaluation['User']['username']); ?></a></li>

		</ul>
		<ul class="bottom_meta">
		<li itemprop="dateCreated" class="meta_date"><i class="icon-time"></i>Posted: Oct 1, 2013</li>
		</ul>
		<a href="http://themes.themeton.com/mana/type/gallery/" title="Gallery posts" class="entry_format"></a>
		</div>
		<div class="entry_content">
		<p><?php echo substr($evaluation['Evaluation']['comments'], 0, 300); ?></p>

		<?php echo $this->Html->link(__('Ler mais...'), array('controller' => 'PubEvaluations', 'action' => 'edit',  $evaluation['Evaluation']['id'])); ?>		

		</div>
		</article>	

	<?php endforeach; ?>

</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Nova Avaliação'), array('action' => 'add')); ?></li>
	</ul>
</div>
