<div>
	<div class="box-title">
	  <h3>
	    <i class="icon-table"></i>
	     Cadastrar Usuário
	  </h3>
	</div>

	<?php echo $this->Form->create('User'); ?>
		<fieldset>
		
			<div class="control-group">
				<?php echo $this->Form->input('username', array('class'=>"input-block-level",'placeholder'=>"Login",
														'label'=>"Login", 'div'=>array('class'=>"pw controls"))); ?>			
			</div>
			<div class="control-group">
				<?php echo $this->Form->input('password', array('class'=>"input-block-level",'placeholder'=>"Senha",
														'label'=>"Senha", 'div'=>array('class'=>"pw controls"))); ?>					
			</div>
			<div class="control-group">
				<?php echo $this->Form->input('role',  array(
		            'class'=>"input-block-level",'placeholder'=>"Senha",
		            'options' => array('client' => 'Client'), 
		            'label' => 'Perfil')); ?>
	        </div>
		
		</fieldset>

	<div class="submit">
		<?php echo $this->Form->end(array('class'=>"btn btn-primary", 'label'=>'Salvar')); ?>
	</div>
		

</div>