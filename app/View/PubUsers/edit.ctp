<div>
	<div class="box-title">
	  <h3>
	    <i class="icon-table"></i>
	     Editar Usuário
	  </h3>
	</div>

	<?php echo $this->Form->create('User', array('url' => array('controller' => 'PubUsers', 'action' => 'edit'))); ?>
	<fieldset>

		<div class="control-group">
			<?php echo $this->Form->input('id', array('type' => 'hidden')); ?>
		</div>
		<div class="control-group">
			<?php echo $this->Form->input('username', array('class'=>"input-block-level",'placeholder'=>"Login",
														'label'=>"Login", 'div'=>array('class'=>"pw controls"))); ?>			
		</div>
		<div class="control-group">
				<?php echo $this->Form->input('password', array('class'=>"input-block-level",'placeholder'=>"Senha",
														'label'=>"Senha", 'div'=>array('class'=>"pw controls"),
														'value'=>'')); ?>	
		</div>
		<div class="control-group">
			<?php echo $this->Form->input('role',  array(
	            'class'=>"input-block-level",'placeholder'=>"Senha",
	            'options' => array('admin' => 'Admin', 'company' => 'Company'), 
	            'label' => 'Perfil')); ?>
        </div>

	</fieldset>

	<div class="submit">
		<?php echo $this->Form->end(array('class'=>"btn btn-primary", 'label'=>'Salvar')); ?>
	</div>


	<div class="actions">
		<h3><?php echo __('Ações'); ?></h3>
		<ul>

			<li><?php echo $this->Form->postLink(__('Excluir'), array('controller' => 'PubUsers', 'action' => 'delete', $this->Form->value('User.id')), null, __('Deseja excluir o regitro # %s?', $this->Form->value('User.id'))); ?></li>
			<li><?php echo $this->Html->link(__('Listar Usuários'), array('controller' => 'PubUsers', 'action' => 'index')); ?></li>
		</ul>
	</div>
</div>